[![Build Status](https://travis-ci.org/blockchain-certificates/cert-issuer.svg?branch=master)](https://travis-ci.org/blockchain-certificates/cert-issuer)
[![PyPI version](https://badge.fury.io/py/cert-issuer.svg)](https://badge.fury.io/py/cert-issuer)

# Original Source Code

Please see the original source code at https://github.com/blockchain-certificates/cert-issuer open sourced by MIT(https://www.blockcerts.org/)

The code here is a customized version for EY Blockathon. A working POC on Identity Management i.e storing individual credentials in the bitcoin blockchain 
and verifying the stored credentials.


# cert-issuer

The cert-issuer project issues blockchain certificates by creating a transaction from the issuing institution to the
recipient on the Bitcoin blockchain that includes the hash of the certificate itself. 

# Quick start using Docker

## Getting the Docker image
This uses bitcoind in TESTNET mode. This route makes many simplifications to allow a quick start, and is intended for
experimenting only.

1. First ensure you have Docker installed. [See our Docker installation help](docs/docker_install.md).

2. Clone the repo and change to the directory

    ```
    git clone https://bitbucket.org/rajesh_kumar_15/blockchain-blockcert.git && cd blockchain-blockcert/
    ```
# steps : Use sudo if necessary...
1. Build the docker image using the below command.
```
docker build -t flaskbc/cert-issuer:1.0 .
```
While building, you will get a red color error line as shown below - Safely ignore that..... 
merkletools 1.0.2 has requirement pysha3==1.0b1, but you'll have pysha3 1.0.2 which is incompatible.

2. Run the docker and attach port 8081 from host to same port in container.
```
docker run -it --entrypoint=/bin/bash -p 8081:8081 flaskbc/cert-issuer:1.0
```

# in bash command type - start the bitcoind daemon as below.
```
bitcoind -daemon
```

# Copy conf.ini and private key for digital signing
```
cp /cert-issuer/conf.ini /etc/cert-issuer/conf.ini
cp /cert-issuer/etc/cert-issuer/pk_issuer.txt /etc/cert-issuer/pk_issuer.txt
```

# from testnet faucet, get some bitcoins for the public address - msA6jaVkGR4g7M7xxz3HK4V63x9qBgMNnh
from the site https://testnet.manu.backend.hamburg/faucet

# start the web server
in the bash command prompt, check the present working directory and see if you are in root. 
then switch to cert-issuer folder using 
```
cd cert-issuer/

bash-4.3# python3 app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:8081/ (Press CTRL+C to quit)
```
Ctrl C and exit to come out of docker

# How batch issuing works

While it is possible to issue one certificate with one Bitcoin transaction, it is far more efficient to use one Bitcoin transaction to issue a batch of certificates. 

The issuer builds a Merkle tree of certificate hashes and registers the Merkle root as the OP_RETURN field in the Bitcoin transaction. 

Suppose the batch contains `n` certificates, and certificate `i` contains recipient `i`'s information. The issuer hashes each certificate and combines them into a Merkle tree:

![](img/merkle.png)


The root of the Merkle tree, which is a 256-bit hash, is issued on the Bitcoin blockchain. The complete Bitcoin transaction outputs are described in 'Transaction structure'.

The Blockchain Certificate given to recipient `i` contains a [2017 Merkle Proof Signature Suite](https://w3c-dvcg.github.io/lds-merkleproof2017/)-formatted signature, proving that certificate `i` is contained in the Merkle tree. 

![](img/blockchain_certificate_components.png)

This receipt contains:

*   The Bitcoin transaction ID storing the Merkle root
*   The expected Merkle root on the blockchain
*   The expected hash for recipient `i`'s certificate
*   The Merkle path from recipient `i`'s certificate to the Merkle root, i.e. the path highlighted in orange above. `h_i -> … -> Merkle root`

The [verification process](https://github.com/blockchain-certificates/cert-verifier-js#verification-process) performs computations to check that:

*   The hash of certificate `i` matches the value in the receipt
*   The Merkle path is valid
*   The Merkle root stored on the blockchain matches the value in the receipt

These steps establish that the certificate has not been tampered with since it was issued.

## Hashing a certificate

The Blockchain Certificate JSON contents without the `signature` node is the certificate that the issuer created. This is the value needed to hash for comparison against the receipt. Because there are no guarantees about ordering or formatting of JSON, first canonicalize the certificate (without the `signature`) against the JSON LD schema. This allows us to obtain a deterministic hash across platforms.

The detailed steps are described in the [verification process](https://github.com/blockchain-certificates/cert-verifier-js#verification-process).


## What should be in a batch?

How a batch is defined can vary, but it should be defined such that it changes infrequently. For example, “2016 MIT grads” would be preferred over “MIT grads” (the latter would have to be updated every year). The size of the batch is limited by the 100KB maximum transaction size imposed by the Bitcoin network. This will amount to a maximum of around 2,000 recipients per certificate batch.

## Transaction structure


One Bitcoin transaction is performed for every batch of certificates. There is no limit to the number of certificates that may be included in a batch, so typically batches are defined in logical groups such as "Graduates of Fall 2017 Robotics Class".

![](/img/tx_out.png)


The transaction structure is the following:

*   Input:
    *    Minimal amount of bitcoin (currently ~$.80 USD) from Issuer's Bitcoin address
*   Outputs:
    *   OP_RETURN field, storing a hash of the batch of certificates
    *   Optional: change to an issuer address

The OP_RETURN output is used to prove the validity of the certificate batch. This output stores data, which is the hash of the Merkle root of the certificate batch. At any time, we can look up this value on the blockchain to help confirm a claim.

The Issuer Bitcoin address and timestamp from the transaction are also critical for the verification process. These are used to check the authenticity of the claim, as described in [verification process](https://github.com/blockchain-certificates/cert-verifier-js#verification-process).

Contact :
Shreeraam Hariharan <Shreeraam.Hariharan@in.ey.com>; Vikraman Narasimhan <vikraman.narasimhan@in.ey.com>; Krishnan Jagadesan <Krishnan.Jagadesan@in.ey.com>; Pankaja Patel <Pankaja.Patel@in.ey.com>; Rajesh Kumar S <Rajesh.Kumar15@in.ey.com>; Abhijan Guha <Abhijan.Guha@in.ey.com>
Priyanka Gupta <Priyanka.Gupta1@in.ey.com>